CREATE TABLE application (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL DEFAULT '',
  email varchar(128) NOT NULL DEFAULT '',
  date int(4) NOT NULL  ,
  gender text(4) NOT NULL DEFAULT '',
  limb int(8) NOT NULL DEFAULT 0,
  super text(128) NOT NULL ,
  biography text(128) NOT NULL ,
  checkon text(8) NOT NULL DEFAULT 'no',
  PRIMARY KEY (id)
);
